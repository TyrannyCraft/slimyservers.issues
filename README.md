# Welcome to Slimy Servers Project

High performance server daemon.

## Create an issue report at https://bitbucket.org/TyrannyCraft/slimyservers.issues/issues

###Free Trial!
http://slimyservers.com/trial


-----------
##Features

* Scalable 
* Fast
* Automatic notifications when a server goes down
* Automatic restarts
* Extensive API
* Much more!
-----------
##Setup
  Goto http://slimyservers.com/setup